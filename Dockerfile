# Utilisez une image Python 3.8
FROM python:3.8

# Définissez le répertoire de travail
WORKDIR /app

# Copiez le fichier requirements.txt pour installer les dépendances Python
COPY requirements.txt /app/requirements.txt

# Affichez le contenu du fichier requirements.txt à des fins de débogage
RUN cat /app/requirements.txt

# Installez les dépendances système nécessaires pour mysqlclient
RUN apt-get update && \
    apt-get install -y \
        default-libmysqlclient-dev \
        build-essential \
    && rm -rf /var/lib/apt/lists/*

# Installez les dépendances Python
RUN pip install --no-cache-dir -r requirements.txt

# Copiez le reste du code dans le conteneur
COPY . /app

# Affichez le contenu du répertoire à des fins de débogage
RUN ls -al /app

# Commande pour démarrer l'application
CMD ["python", "app.py"]
